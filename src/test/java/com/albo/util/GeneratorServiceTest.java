package com.albo.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class GeneratorServiceTest {

    @Test
    void returnLetterCode() throws Exception {
        GeneratorService service = new GeneratorService();
        String actualLetter = service.getLetterCode(65);
        assertEquals("A", actualLetter);
    }

    @Test
    void shouldGetLetterFromKnownIndex() {
        GeneratorService service = new GeneratorService();
        int actualIndex = service.getActualAsciiFromIndex(1);
        assertEquals(65, actualIndex);
        actualIndex = service.getActualAsciiFromIndex(5);
        assertEquals(69, actualIndex);
    }

    @Test
    void shouldGetNumberCodeForLetters() {
        GeneratorService service = new GeneratorService();
        int actualCode = service.getCodeFromCharacter("A");
        assertEquals(65, actualCode);
        
        actualCode = service.getCodeFromCharacter("AA");
        assertEquals(91, actualCode);
        
        actualCode = service.getCodeFromCharacter("AM");
        assertEquals(103, actualCode);
    }

    @Test
    void shouldGetArrayFromRange() {
        GeneratorService service = new GeneratorService();
        String[] letters = service.getLettersArray(88, 25);
        printArray(letters);
        assertEquals("X", letters[0]);
        assertEquals("AB", letters[4]);
        assertEquals("AV", letters[24]);
    }

    @Test
    void shouldGetLettersFromCode() {
        GeneratorService service = new GeneratorService();
        String letter = service.getLettersFromCode(66);
        assertEquals("B", letter);
        letter = service.getLettersFromCode(90);
        assertEquals("Z", letter);
        letter = service.getLettersFromCode(91);
        assertEquals("AA", letter);
    }

    @Test
    void shouldManageListStartFromA() {
        GeneratorService service = new GeneratorService();
        List<String> fieldNames = Arrays.asList("field1", "field2", "field3");
        List<MessagesHelper> helpers = service.getHelpers(fieldNames, "A");
        assertEquals(fieldNames.size() * 2, helpers.size(), "array size incorrect");
        assertEquals(fieldNames.get(1), helpers.get(3).getField().getName());
        assertEquals("B", helpers.get(1).getCharCode());
        assertEquals(66, helpers.get(1).getIndex());
    }

    @Test
    void shouldManageListStartFromY() {
        GeneratorService service = new GeneratorService();
        List<String> fieldNames = Arrays.asList("field1", "field2", "field3");
        List<MessagesHelper> helpers = service.getHelpers(fieldNames, "Y");
        assertEquals(fieldNames.size() * 2, helpers.size());
        assertEquals(fieldNames.get(1), helpers.get(3).getField().getName());
        assertEquals("Z", helpers.get(1).getCharCode());
        
        assertEquals(fieldNames.get(2), helpers.get(5).getField().getName());
        assertEquals("AA", helpers.get(2).getCharCode());
    }

    void printArray(Object[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

}
