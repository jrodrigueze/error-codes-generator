package com.albo.util;

public class MessagesHelper {

    private Field field;
    private ErrorTypes errorType;

    private int index;
    private String charCode;


    public MessagesHelper(Field field, int index, String charCode, ErrorTypes errorType) {
        this.field = field;
        this.index = index;
        this.charCode = charCode;
        this.errorType = errorType;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public ErrorTypes getErrorType() {
        return errorType;
    }
    
    public void setErrorType(ErrorTypes errorType) {
        this.errorType = errorType;
    }

    String getMessageCode() {
        String message = "        public static final String CODE_200_" + charCode + " = \"200-"
                + charCode + "\";";
        return message;
    }

    String getMessageDesc() {
        String error = "";
        switch(errorType){
            case NOT_FOUND:
                error = " value was not found";
                break;
            case EMPTY:
                error = " can not be empty";
                break;
        }
        String message = "        public static final String DESCRIPTION_" + charCode + " = \"The "
                + field.getFormattedName() + error + "\";";
        return message;
    }

    String getMessageCodeNt() {
        String error = "";
        switch(errorType){
            case NOT_FOUND:
                error = "001";
                break;
            case EMPTY:
                error = "002";
                break;
        }
        String message = "        public static final String CODE_200_" + charCode + "_NT = \"" + error + "\";";
        return message;
    }

    String getMessageDetail() {
        String error = "";
        switch(errorType){
            case NOT_FOUND:
                error = "_not_found";
                break;
            case EMPTY:
                error = "_can_not_be_empty";
                break;
        }
        String message = "        public static final String DETAIL_200_" + charCode + " = \"" + field.getPath() + "::"
                + field.getName() + error + "\";";
        return message;
    }

    String getMessageExample() {
        String message = "        public static final String EXAMPLE_" + charCode + " = \"{\\n\" + \n " +
            "               \"    \\\"data\\\": {\\n\" + \n " +
            "               \"        \\\"exception\\\": {\\n\" + \n " +
            "               \"            \\\"code\\\": \\\"\" + CODE_SERVICE + CODE_200_" + charCode + "_NT + \"\\\",\\n\" + \n " +
            "               \"            \\\"detail\\\": \\\"\" + DETAIL_200_" + charCode + " + \"\\\"\\n\" + \n " +
            "               \"        }\\n\" + \n " +
            "               \"    },\\n\" + \n " +
            "               \"    \\\"response\\\": \\\"BAD\\\"\\n\" + \n " +
            "               \"}\";";
        return message;
    }

    String getMessageApiResponse() {
        String message = "            @APIResponse(responseCode = CODE_200_" + charCode + ", description = DESCRIPTION_" + charCode + ", content = @Content(mediaType = MediaType.APPLICATION_JSON, example = EXAMPLE_" + charCode + ")), ";
        return message;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCharCode() {
        return charCode;
    }

    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }
}
