package com.albo.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeneratorService {

    String INITIAL_LETTER = "A";
    String FINAL_LETTER = "Z";

    int INITIAL_LETTER_INDEX = 65;
    int FINAL_LETTER_INDEX = 90;

    int currentIndex = INITIAL_LETTER_INDEX;

    void execute(List<String> theList, String initialChar) {
        System.out.println("-------------------------------------------------\n");
        List<MessagesHelper> helpers = getHelpers(theList, initialChar);

        printAllMessages(helpers);
        System.out.println("\n\n\n\n\n-------------------------------------------------\n\n\n\n\n\n");
        printAllApiResponses(helpers);
    }

    void printAllMessages(List<MessagesHelper> helpers) {
        for (MessagesHelper helper : helpers) {
            printMessages(helper);
        }
    }

    void printAllApiResponses(List<MessagesHelper> helpers) {
        for (MessagesHelper helper : helpers) {
            printApiResponses(helper);
        }
    }

    void printMessages(MessagesHelper helper) {
        System.out.println(helper.getMessageCode());
        System.out.println(helper.getMessageDesc());
        System.out.println(helper.getMessageCodeNt());
        System.out.println(helper.getMessageDetail());
        System.out.println(helper.getMessageExample());
    }

    void printApiResponses(MessagesHelper helper) {
        System.out.println(helper.getMessageApiResponse());
    }

    List<MessagesHelper> getHelpers(List<String> theList, String initialChar) {
        int initialIndex = getCodeFromCharacter(initialChar);
        List<MessagesHelper> helperList = new ArrayList<>();
        List<ErrorTypes> errors = Arrays.asList(ErrorTypes.values());

        for (String fieldString : theList) {
            Field field = new Field(fieldString);

            for (ErrorTypes error : errors) {
                int fieldIndex = initialIndex++;
                String fieldCharCode = getLettersFromCode(fieldIndex);
                helperList.add(new MessagesHelper(field, fieldIndex, fieldCharCode, error));
            }

        }
        return helperList;
    }

    String getLetterCode(int index) {
        return Character.valueOf(getLetterFromIndex(index)).toString();
    }

    String[] getLettersArray(int initialIndex, int numberOfLetters) {
        List<String> letters = new ArrayList<String>();
        for (int i = initialIndex; i <= (initialIndex + numberOfLetters - 1); i++) {
            String theCode = getLettersFromCode(i);
            letters.add(theCode);
        }
        String[] theArray = new String[letters.size()];
        return letters.toArray(theArray);
    }

    String getLettersFromCode(int code) {
        StringBuilder letters = new StringBuilder();

        int firstCode = code - INITIAL_LETTER_INDEX + 1;
        if (code - INITIAL_LETTER_INDEX > getRangeCount()) {
            firstCode = (code - INITIAL_LETTER_INDEX + 1) / getRangeCount();
        }
        firstCode = getActualAsciiFromIndex(firstCode);
        letters.append(getLetterCode(firstCode));

        if (code > FINAL_LETTER_INDEX) {
            code = (code - INITIAL_LETTER_INDEX) % getRangeCount();
            code = getActualAsciiFromIndex(code);
            letters.append(getLetterCode(code));
        }
        return letters.toString();
    }

    int getRangeCount() {
        return FINAL_LETTER_INDEX - INITIAL_LETTER_INDEX;
    }

    char getLetterFromIndex(int index) {
        return (char) index;
    }

    int getActualAsciiFromIndex(int index) {
        return index + INITIAL_LETTER_INDEX - 1;
    }

    void generate() {
        int initialLetter = getCodeFromCharacter(INITIAL_LETTER);
        int finalLetter = getCodeFromCharacter(FINAL_LETTER);

        for (int i = initialLetter; i <= finalLetter; i++) {
            System.out.println((char) i);
        }
    }

    int getCodeFromCharacter(String strings) throws IllegalArgumentException {

        if (strings == null || strings.length() > 2) {
            throw new IllegalArgumentException("Invalid String. Can't be null and lenght can exceed 2 characters");
        }

        int code = 0;

        if (strings.length() == 1) {
            char letter = strings.charAt(0);
            validateChar(letter);
            code = letter;
        } else if (strings.length() == 2) {
            char[] theLetters = strings.toCharArray();
            for (char letter : theLetters) {
                validateChar(letter);
            }
            int finalCode = strings.charAt(0);
            finalCode = finalCode - INITIAL_LETTER_INDEX + 1;
            finalCode = finalCode * INITIAL_LETTER_INDEX;
            int secondChar = (strings.charAt(1) - INITIAL_LETTER_INDEX + 1) + getRangeCount();
            finalCode = finalCode + (secondChar);
            code = finalCode;
        }
        return code;
    }

    void validateChar(char theChar) throws IllegalArgumentException {
        int code = (int) theChar;
        if (code < INITIAL_LETTER_INDEX || code > FINAL_LETTER_INDEX) {
            throw new IllegalArgumentException("Invalid char. Must be from A to Z, uppercase");
        }
    }
}
