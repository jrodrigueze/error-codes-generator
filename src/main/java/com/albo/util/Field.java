package com.albo.util;

public class Field {

    private String completeName;
    private String basePath = "data";
    private String path;
    private String name;

    public Field(String completeName) {
        this.completeName = completeName;
        setNameParts(completeName);
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String name) {
        this.completeName = name;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public String getFormattedName() {
        String[] parts = name.split("_");
        StringBuilder formattedName = new StringBuilder();

        for (String part : parts) {
            formattedName.append(part.substring(0, 1).toUpperCase());
            formattedName.append(part.substring(1).toLowerCase());
            formattedName.append(" ");
        }
        return formattedName.toString().trim();
    }

    void setNameParts(String completeName) {
        StringBuilder thePath = new StringBuilder();
        thePath.append(basePath);
        if (completeName.contains(".")) {
            thePath.append(".");
            thePath.append(completeName.substring(0, completeName.indexOf(".")));
        }
        if (completeName.contains(".")) {
            name = completeName.substring(completeName.lastIndexOf(".") + 1);
        } else {
            name = completeName;
        }

        path = thePath.toString();
    }
}
