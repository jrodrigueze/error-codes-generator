package com.albo.util;

import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<String> fieldNames = Arrays.asList("entity_type", "entity.uuid",
                "entity.tracking_key", "entity.space_uuid", "entity.type", "entity.amount", "entity.concept",
                "entity.reference", "entity.status", "entity.operation_executed", "entity.order_uuid");
        GeneratorService service = new GeneratorService();
        service.execute(fieldNames, "D");
    }
}
