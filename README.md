# error-codes-generator

Este proyecto es un generador de códigos de error para validaciones de payloads de entrada. Permite la generación de códigos, descripciones y ejemplos de error por validaciones de existencia y valor no nulo para los campos que se proporcionen.